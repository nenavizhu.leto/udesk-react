import { ReactNode } from "react"

function Sidebar({ children }: { children: ReactNode }) {

	return (
		<div className="w-[72px] h-screen pt-4 flex-col justify-start items-start gap-4 inline-flex border-r-gray-200 border">
			{children}
		</div>
	)
}

export default Object.assign(Sidebar, {
	Logo: function({ icon }: { icon: string }) {
		return (
			<div className="self-stretch h-8 flex-col justify-start items-center gap-2.5 flex">
				<div className="w-8 h-8 justify-center items-center inline-flex">
					<div className="w-8 h-8 relative rounded flex-col justify-start items-start flex" />
					<img src={icon} />
				</div>
			</div>
		)
	},
	Group: function({ children }: { children: ReactNode }) {
		return (
			<div className="self-stretch h-[120px] px-4 py-3 flex-col justify-start items-center gap-4 flex">
				{children}
			</div>
		)
	},
	Bottom: function({ children }: { children: ReactNode }) {
		return (
			<div className="self-stretch grow shrink basis-0 py-4 flex-col justify-end items-center gap-4 flex">
				{children}
			</div>
		)
	},
	Hr: function() {
		return (<div className="self-stretch h-px bg-gray-200" />)
	}
})
