import { Modal, Tooltip, Button } from "flowbite-react"
import { useState } from "react"

import { HiOutlineFilm } from "react-icons/hi"

const telegram = {
	pass: 'pass'
}

export default function TelegramWidget() {
	const [show, setShow] = useState(false);

	return (
		<>
			<Modal title="Доступ к телеграм-боту" size="sm" show={show}>
				<p className="text-base leading-relaxed text-gray-500 dark:text-gray-400">
					Для получения уведомений по обращениям скопируйте и отправьте этот код
					доступа телеграм-боту:
				</p>
				<div>
					<Tooltip trigger="click" placement="right" content={"Скопировано"}>
						<Tooltip trigger="hover" placement="right" content={"Скопировать код"}>
							<button id="copy" onClick={() => setTimeout(() => setShow(false), 1000)} className="inline-flex items-center self-start gap-2 cursor-pointer text-lg font-semibold leading-relaxed text-gray-500 dark:text-gray-400">{telegram?.pass}
								<HiOutlineFilm />
							</button>
						</Tooltip>
					</Tooltip>
				</div >
				<Button color="blue" href="https://t.me/HDNotificator_bot">Перейти в телеграм-бота</Button>
			</Modal>

			<h2 className="text-black text-2xl font-semibold mb-6">Уведомления в Телеграм</h2>

			<div className="flex flex-col gap-6">
				<div className="w-96 text-gray-500 text-base font-normal leading-normal">
					У вас уже подключен телеграм-бот для уведомений по обращениям
				</div>
				<div className="flex gap-4">
					<Button color="blue" href="https://t.me/HDNotificator_bot">Перейти в телеграм-бота</Button>
					<Button color="blue" outline onClick={() => setShow(true)}>Показать пароль</Button>
				</div>
				<div className="w-96 text-gray-500 text-base font-normal leading-normal">
					Активируйте телеграм-бота и получайте уведомления по обращениям прямо в
					приложение телеграм
				</div>
				<Button className="self-start" color="blue">Активировать телеграм-бота</Button>
			</div>

		</>
	)
}
