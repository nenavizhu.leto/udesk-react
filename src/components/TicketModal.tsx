import { Alert, Button, FileInput, Label, Modal, TextInput, Textarea } from "flowbite-react";
import { BadgeCheckOI, ExclamationCircleOI } from "flowbite-react-icons";
import React, { useState } from "react";
import { HiOutlineExclamationCircle } from "react-icons/hi";


function TicketModalCreate({ setState }: { setState: React.Dispatch<any> }) {
	return (
		<>
			<Modal.Header>
				<h3 className="mb-4 text-xl font-medium text-gray-900 dark:text-white">
					Создание обращения
				</h3>
				<Alert color="blue">
					Опишите в поле «Тема обращения» краткое содержание обращения, в поле
					«Суть обращения» опишите проблему подробно.
				</Alert>
			</Modal.Header>
			<Modal.Body>
				<form className="flex flex-col gap-4">
					<div>
						<div className="mb-2">
							<Label value="Тема обращения" />
						</div>
						<TextInput
							type="text"
							name="name"
							placeholder="Поломка принтера"
							required
							sizing="md"
						/>
					</div>
					<div>
						<div className="mb-2">
							<Label value="Суть обращения" />
						</div>
						<Textarea
							name="subject"
							rows={6}
							style={{ "resize": "none" }}
							placeholder="Подробное описание проблемы"
							required
						/>
					</div>
					<div>
						<div className="mb-2">
							<Label value="Прикрепить файлы" />
						</div>
						<FileInput helperText=".png, .jpeg или .jpg (максимальный размер 2мб)" />
					</div>
				</form>
			</Modal.Body>
			<Modal.Footer>
				<Button color="blue" type="submit" className="" size="lg" onClick={() => setState("success")}>
					Создать обращение
				</Button>
			</Modal.Footer>
		</>
	)
}

function TicketModalSuccess({ close }: { close: () => void }) {
	return (
		<>
			<Modal.Body className="mt-14 text-center">
				<BadgeCheckOI className="mx-auto mb-4 text-gray-400 w-12 h-12" />
				<h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
					Обращение успешно создано. <br />Специалисты проверят корректность данных и начнут работу в ближайшее время.
				</h3>
			</Modal.Body>
			<Modal.Footer className="self-center">
				<Button color="blue" className="mr-2" onClick={close}>
					Закрыть
				</Button>
				<Button color="blue" outline className="mr-2">
					Перейти в обращение
				</Button>
			</Modal.Footer>
		</>
	)

}

function TicketModalError({ close }: { close: () => void }) {

	return (
		<>
			<HiOutlineExclamationCircle
				className="mx-auto mb-4 text-gray-400 w-12 h-12 dark:text-gray-200"
			/>
			<h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
				Не удалось создать обращение.
			</h3>
			<Button color="blue" className="mr-2" onClick={close}>
				OK
			</Button>
		</>
	)
}


function TicketModalClose({ setState, close }: { setState: React.Dispatch<any>, close: () => void }) {
	return (
		<>
			<Modal.Body className="mt-14 text-center">
				<ExclamationCircleOI className="mx-auto mb-4 text-gray-400 w-12 h-12" />
				<h3 className="mb-5 text-lg font-normal text-gray-500">
					Закрыть создание обращения? <br />Введенные данные не сохранятся.
				</h3>
			</Modal.Body>
			<Modal.Footer className="self-center">
				<Button color="blue" className="mr-2" onClick={close}>
					Закрыть
				</Button>
				<Button color="blue" outline onClick={() => setState("create")}>
					Отмена
				</Button>
			</Modal.Footer>
		</>
	)
}

type TicketModalState = "create" | "close" | "success" | "error";

export default function TicketModal({ open, close }: { open: boolean, close: () => void }) {
	const [state, setState] = useState<TicketModalState>("create")

	return (
		<Modal show={open} dismissible={false} onClose={() => setState("close")}>
			{state == "create" && <TicketModalCreate setState={setState} />}
			{state == "success" && <TicketModalSuccess close={close} />}
			{state == "error" && <TicketModalError close={close} />}
			{state == "close" && <TicketModalClose close={close} setState={setState} />}
		</Modal>
	)
}
