import {Badge, Table} from "flowbite-react";
import {CheckCircleSI, ClockTimeSI, EyeSI, FireSI, ExclamationCircleSI} from "flowbite-react-icons";

export function TaskStatus({status}: {status: string}) {
	let color = "indigo";
	let content = "На проверке";
	let icon = <EyeSI size="xs" />;
	switch (status) {
		case "accepted":
			color = "yellow";
			content = "В работе";
			icon = <FireSI size="xs" />;
			break;
		case "waiting":
			color = "purple";
			content = "Требует принятия";
			icon = <ClockTimeSI size="xs" />;
			break;
		case "done":
			color = "green";
			content = "Завершено";
			icon = <CheckCircleSI size="xs" />;
			break;
		case "cancelled":
			color = "red";
			content = "Отклонено";
			icon = <ExclamationCircleSI size="xs" />;
			break;
	}

	return (
		<Badge color={color} className="w-fit">
			<span className="inline-flex gap-1 items-center">
				<span className="shrink-0">
          {icon}
        </span>
				{content}
			</span>
		</Badge>
	);
}
function TaskTableRow({task}: {task: any}) {
	return (
		<Table.Row>
			<Table.Cell>{task.id}</Table.Cell>
			<Table.Cell>{task.name}</Table.Cell>
			<Table.Cell>
				<TaskStatus status={task.status} />
			</Table.Cell>
			<Table.Cell>{task.created_at.toString()}</Table.Cell>
		</Table.Row>
	);
}

export function UserTaskTable({tasks}: {tasks: any[]}) {
	return (
		<>
			<Table hoverable>
				<Table.Head>
					<Table.HeadCell>Номер</Table.HeadCell>
					<Table.HeadCell className="w-full">Тема</Table.HeadCell>
					<Table.HeadCell>Статус</Table.HeadCell>
					<Table.HeadCell>Создано</Table.HeadCell>
				</Table.Head>
				<Table.Body className="cursor-pointer">
					{tasks.map((task) => (
						<TaskTableRow task={task} />
					))}
				</Table.Body>
			</Table>
		</>
	);
}
