import { Button } from "flowbite-react"
import { Link } from "react-router-dom"
import SupportAsset from "./assets/SupportAsset.svg"
import { HiArrowRight } from "react-icons/hi"
import { ArrowRightOI } from "flowbite-react-icons"
import Logo from "./assets/logo.svg"

function App() {

	return (
		<div className={"max-w-6xl mx-auto flex items-center justify-between my-60 flex-wrap"}>
			<div className={"flex flex-col"}>
				<div className="mb-12">
					<img src={Logo} className="mb-6" />
					<h1 className={"mb-6 font-bold text-4xl"}>
						Добро пожаловать <br />
						в help-desk!
					</h1>
					<p className={"text-xl"}>
						Это приложение для решения ваших <br />
						технических проблем в офисе
					</p>

				</div>
				<Link to={`/system`}>
					<Button color="blue" size={"xl"}>
						Начать работу
						<ArrowRightOI size="sm" className="ml-2" />
					</Button>
				</Link>
			</div>
			<img src={SupportAsset} />
		</div>
	)
}

export default App
