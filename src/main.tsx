import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import {
	createBrowserRouter,
	RouterProvider,
} from "react-router-dom"
import './index.css'

import System from './pages/System.tsx'
import Profile from './pages/Profile.tsx'
import Tasks from './pages/Tasks.tsx'
import Error from './pages/Error.tsx'
import Register from './pages/Register.tsx'
import Task from './pages/Task.tsx'

const router = createBrowserRouter([
	{
		path: "/",
		element: <App />,
		errorElement: <Error />,
	},
	{
		path: "/register",
		element: <Register />
	},
	{
		path: "/system",
		element: <System />,
		errorElement: <Error />,
		children: [
			{ index: true, element: <Profile /> },
			{
				path: "tasks",
				element: <Tasks />,
			},
			{
				path: "/system/tasks/:id",
				element: <Task />
			}
		]
	}
])

ReactDOM.createRoot(document.getElementById('root')!).render(
	<React.StrictMode>
		<RouterProvider router={router} />
	</React.StrictMode>,
)
