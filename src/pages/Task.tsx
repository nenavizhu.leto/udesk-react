import {Avatar, Breadcrumb} from "flowbite-react";
import {ClipboardCheckSI} from "flowbite-react-icons";
import {TaskStatus} from "../components/UserTaskTable";
import {Timeline} from "flowbite-react";
import {HiCalendar} from "react-icons/hi";

function Breadcrumbs({id}: {id: number}) {
	return (
		<div>
			<Breadcrumb>
				<Breadcrumb.Item href="/system/tasks" icon={ClipboardCheckSI}>
					Обращения
				</Breadcrumb.Item>
				<Breadcrumb.Item>#{id}</Breadcrumb.Item>
			</Breadcrumb>
		</div>
	);
}

function Info({
	task,
}: {
	task: {
		name: string;
		status: string;
		date: string;
		employee: string;
		id: string;
		user: string;
		desc: string;
	};
}) {
	return (
		<div className="w-full max- py-6 flex flex-col gap-6 items-start">
			<div className="self-stretch gap-4flex">
				<h1 className="text-2xl font-semibold leading-normal">
					Поломка принтера
				</h1>
			</div>
			<TaskStatus status={task.status} />

			<div className="self-stretch items-start gap-4 flex leading-none">
				<div className="w-64 shrin text-sm font-semibold leading-none">
					Номер обращения
				</div>
				<div className="grow shrink text-sm font-normal leading-none">
					{task.id}
				</div>
			</div>
			<div className="self-stretch items-start gap-4 flex leading-none">
				<div className="w-64 shrin text-sm font-semibold leading-none">
					Постановщик
				</div>
				<div className="grow shrink text-sm font-normal leading-none">
					{task.user}
				</div>
			</div>
			<div className="self-stretch items-start gap-4 flex leading-none">
				<div className="w-[16rem] shrin text-sm font-semibold leading-none">
					Дата постановки
				</div>
				<div className="grow shrink text-sm font-normal leading-none">
					{task.date}
				</div>
			</div>
			<div className="self-stretch items-start gap-4 flex leading-none">
				<div className="w-64 shrin text-sm font-semibold leading-none">
					Технический специалист
				</div>
				<div className="grow shrink text-sm font-normal leading-none">
					{task.employee}
				</div>
			</div>
			<div className="self-stretch h-px border border-gray-200"></div>
			<div className="self-stretch items-start gap-4 flex flex-col leading-none">
				<div className="w-64 shrin text-sm font-semibold leading-none">
					Описание
				</div>
				<div className="grow shrink text-sm font-normal leading-normal">
					{task.desc}
				</div>
			</div>
			<div className="self-stretch h-px border border-gray-200"></div>
		</div>
	);
}

function History() {
	return (
		<div className="flex flex-col gap-6">
			<div className="w-64 shrin text-sm font-semibold leading-none">
				История обращения
			</div>
      {/* History List */}
     <div className="ticket-history flex flex-col gap-6">
      {/* Item */}
       <div className="flex gap-2">
          <div className="w-8 h-8 flex items-center justify-center">
            <div className="bg-gray-200 rounded-full w-[12px] h-[12px] relative">
              <div className="bg-gray-500 rounded-full w-[6px] h-[6px] absolute top-[3px] left-[3px]"></div>
            </div>
          </div>
    			<div className=" w-full flex flex-col ">
    				<div className="p-4 bg-gray-100 rounded-lg justify-start items-start gap-3 flex">
    					<div className="items-center gap-2 flex">
    						<div className="text-sm font-semibold leading-none">
    							Александр Сулимко
    						</div>
    						<div className="text-gray-500 text-sm font-normal leading-none">
    							назначен(а) исполнителем обращения
    						</div>
    					</div>
    					<div className="grow shrink basis-0 self-stretch justify-end items-center gap-2.5 flex">
    						<div className="text-gray-500 text-xs font-normal leading-3">
    							22 ноября 2023, 11:36{" "}
    						</div>
    					</div>
    				</div>
    			</div>
       </div>
      {/* Item */}
       <div className="flex gap-2">
          <div className="w-8 h-8 flex items-center justify-center">
            <div className="bg-gray-200 rounded-full w-[12px] h-[12px] relative">
              <div className="bg-gray-500 rounded-full w-[6px] h-[6px] absolute top-[3px] left-[3px]"></div>
            </div>
          </div>
    			<div className=" w-full flex flex-col ">
    				<div className="p-4 bg-gray-100 rounded-lg justify-start items-start gap-3 flex">
    					<div className="items-center gap-2 flex">
    						<div className="text-sm font-semibold leading-none">
    							Александр Сулимко
    						</div>
    						<div className="text-gray-500 text-sm font-normal leading-none">
    							назначен(а) исполнителем обращения
    						</div>
    					</div>
    					<div className="grow shrink basis-0 self-stretch justify-end items-center gap-2.5 flex">
    						<div className="text-gray-500 text-xs font-normal leading-3">
    							22 ноября 2023, 11:36{" "}
    						</div>
    					</div>
    				</div>
    			</div>
       </div>
      {/* Item */}
       <div className="flex gap-2">
          <div className="w-8 h-8 flex items-center justify-center">
            <div className="bg-gray-200 rounded-full w-[12px] h-[12px] relative">
              <div className="bg-gray-500 rounded-full w-[6px] h-[6px] absolute top-[3px] left-[3px]"></div>
            </div>
          </div>
    			<div className=" w-full flex flex-col ">
    				<div className="p-4 bg-gray-100 rounded-lg justify-start items-start gap-3 flex">
    					<div className="items-center gap-2 flex">
    						<div className="text-sm font-semibold leading-none">
    							Александр Сулимко
    						</div>
    						<div className="text-gray-500 text-sm font-normal leading-none">
    							назначен(а) исполнителем обращения
    						</div>
    					</div>
    					<div className="grow shrink basis-0 self-stretch justify-end items-center gap-2.5 flex">
    						<div className="text-gray-500 text-xs font-normal leading-3">
    							22 ноября 2023, 11:36{" "}
    						</div>
    					</div>
    				</div>
    			</div>
       </div>
     </div>
		</div>
	);
}

function MessageInput() {
	return <div></div>;
}

export default function Ticket() {
	return (
		<div className="w-full max-h-screen overflow-y-auto px-6 pt-6">
			<div className="w-full">
				<Breadcrumbs id={1243} />
				<Info
					task={{
						name: "Поломка принтера",
						date: "22 ноября 2023, 11:36 ",
						status: "accepted",
						employee: "Александр Сулимко",
						id: "1243",
						user: "Нателла Наумова",
						desc: `Наша машина, известная как "Принтер Пандемониум", решила устроить карнавал сбоев и абсурда, в котором бумага превратилась в спагетти, а чернила начали исполнять хоровые песни без нашего ведома.`,
					}}
				/>
				<History />
				<MessageInput />
			</div>
		</div>
	);
}