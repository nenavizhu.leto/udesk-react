import { Button, Card, Label, TextInput } from "flowbite-react";
import Logo from "../assets/logo.svg"

export default function Register() {

	return (
		<div className="flex gap-24 justify-center max-w-4xl mx-auto mt-56">
			<div className="self-center">
				<img src={Logo} className="mb-6" />
				<div className="text-4xl font-bold leading-10 mb-6">Вы почти у цели!</div>
				<div className="text-xl font-normal leading-tight">
					Чтобы начать пользоваться приложением, пожалуйста, пройдите быструю и
					простую процедуру регистрации
				</div>
			</div>
			<Card className="w-full max-w-md">
				<form className="flex flex-col space-y-6">
					<h3 className="text-xl font-medium text-gray-900 dark:text-white">
						Регистрация в системе
					</h3>
					<Label className="space-y-2">
						<span>Имя</span>
						<TextInput
							type="text"
							sizing="lg"
							name="first_name"
							placeholder="Нателла"
							required
							maxLength={20}
						/>
					</Label>
					<Label className="space-y-2">
						<span>Фамилия</span>
						<TextInput
							type="text"
							sizing="lg"
							name="last_name"
							placeholder="Наумова"
							required
							maxLength={20}
						/>
					</Label>
					<Label className="space-y-2">
						<span>Внутренний номер</span>
						<TextInput
							type="text"
							sizing="lg"
							name="phone"
							placeholder="1234"
							required
							maxLength={5}
						/>
					</Label>
					<Button color="blue" size={"xl"} className="w-full">Продолжить</Button>
				</form>
			</Card>
		</div>
	)
}
