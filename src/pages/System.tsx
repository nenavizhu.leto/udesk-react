import { Link, Outlet } from "react-router-dom";
import Logo from "../assets/logo.svg"
import { ClipboardListSI, CogSI, EnvelopeSI } from "flowbite-react-icons";
import Sidebar from "../components/Sidebar";


export default function System() {
	return (
		<div className="flex ">
			<Sidebar>
				<Sidebar.Logo icon={Logo} />
				<Sidebar.Group>
					<Link to={"/system/tasks"} className="w-10 h-10 bg-gray-100 rounded-md justify-center items-center inline-flex">
						<ClipboardListSI className="text-gray-500" />
					</Link>
					<Link to={"/system/notifications"} className="w-10 h-10 rounded-md justify-center items-center inline-flex">
						<EnvelopeSI className="text-gray-500" />
					</Link>
				</Sidebar.Group>
				<Sidebar.Hr />
				<Sidebar.Bottom>
					<div className="w-[60px] py-1 justify-center items-center gap-[17px] inline-flex">
						<Link to={"/system/profile"} className="w-8 h-8 bg-gray-100 rounded-[999px] flex-col justify-center items-center gap-2.5 inline-flex">
							<div className="text-center text-gray-900 text-xs font-medium font-['Inter'] leading-3">НН</div>
						</Link>
					</div>
					<Link to={"/system/settings"} className="w-[60px] py-1 justify-center items-center gap-[17px] inline-flex">
						<CogSI className="text-gray-500" />
					</Link>
				</Sidebar.Bottom>
			</Sidebar>
      <div className="w-full max-w-[1920px] mx-auto">
			<Outlet />
      </div>
		</div>
	)
}
