import {
	Badge,
	Button,
	Table,
	TextInput,
	Dropdown,
	DropdownHeader,
	Label,
	Select,
	DropdownItem,
} from "flowbite-react";
import EmptyTickets from "../assets/EmptyTickets.svg";
import {
	CheckCircleOI,
	ChevronLeftOI,
	ChevronRightOI,
	ClipboardListOI,
	ClockTimeOI,
	DotsHorizontalOI,
	EyeOI,
	FilterOI,
	FireOI,
	PlusOI,
	SearchLoopOI,
} from "flowbite-react-icons";
import {ReactNode, useState} from "react";
import {customTableTheme} from "../themes";
import {TaskStatus} from "../components/UserTaskTable";
import TicketModal from "../components/TicketModal";
import {useNavigate} from "react-router-dom";

const tasks: any[] = [
	{
		id: "1234",
		name: "Ошибка при установке нового ПО",
		status: "cancelled",
		created_at: "Apr 23 ,2021",
		notifications_count: 1,
	},
	{
		id: "2333",
		name: "Поломка принтера",
		status: "waiting",
		created_at: "Apr 23 ,2021",
		notifications_count: 2,
	},
	{
		id: "2323",
		name: "Проблемы с подключением к Wi-Fi сети",
		status: "assigned",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "3042",
		name: "Зависание компьютера",
		status: "done",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "1231",
		name: "Не работает мобильное устройство",
		status: "done",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
	{
		id: "2321",
		name: "Проблемы с отправкой электронной почты.",
		status: "accepted",
		created_at: "Apr 23 ,2021",
	},
];

function StatSection({
	children,
	value,
	label,
	color,
}: {
	children: ReactNode;
	value: number;
	label: string;
	color: string;
}) {
	return (
		<div
			className={`flex p-3 bg-${color}-50 rounded-lg gap-3 grow shrink-0 basis-56 items-center border-${color}-200 border`}
		>
			<span className={`p-2 bg-${color}-100 text-${color}-500 rounded-lg`}>
				{children}
			</span>
			<div className="flex flex-col">
				<span className="font-bold text-2xl">{value}</span>
				<span className={`text-${color}-500`}>{label}</span>
			</div>
		</div>
	);
}

const ITEMS_PER_PAGE = 6;
function TablePagination({page, total}: {page: number; total: number}) {
	let pages = Math.ceil(total / ITEMS_PER_PAGE);
	let start = page * ITEMS_PER_PAGE + 1;
	let end = start + ITEMS_PER_PAGE - 1;

	return (
		<nav className="flex items-center justify-between pt-4">
			<span className="flex gap-2 text-sm text-gray-500 mb-4 w-full">
				Показано
				<span className="font-semibold text-gray-900 dark:text-white">
					{start}-{end}
				</span>
				из
				<span className="font-semibold text-gray-900 dark:text-white">
					{total}
				</span>
			</span>
			<ul className="inline-flex -space-x-px rtl:space-x-reverse text-sm h-8">
				<li>
					<a
						href="#"
						className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-s-lg hover:bg-gray-100 hover:text-gray-700"
					>
						<ChevronLeftOI size="xs" />
					</a>
				</li>
				{Array(pages)
					.fill(0)
					.map((_, i) => {
						return (
							<li key={i}>
								<a
									href="#"
									className="flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 hover:bg-gray-100 hover:text-gray-700"
								>
									{i + 1}
								</a>
							</li>
						);
					})}
				<li>
					<a
						href="#"
						className="flex items-center justify-center px-3 h-8 ms-0 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg hover:bg-gray-100 hover:text-gray-700"
					>
						<ChevronRightOI size="xs" />
					</a>
				</li>
			</ul>
		</nav>
	);
}

function TaskTable() {
	const navigate = useNavigate();

	return (
		<Table theme={customTableTheme}>
			<Table.Head className="">
				<Table.HeadCell></Table.HeadCell>
				<Table.HeadCell>Номер</Table.HeadCell>
				<Table.HeadCell className="min-w-full">Тема</Table.HeadCell>
				<Table.HeadCell className="">Статус</Table.HeadCell>
				<Table.HeadCell className="">Создано</Table.HeadCell>
				<Table.HeadCell></Table.HeadCell>
			</Table.Head>
			<Table.Body className="text-black">
				{tasks.map((task) => {
					const ns_count = task?.notifications_count ?? 0;
					const row_bg = task.status == "cancelled" ? "bg-red-50" : "bg-white";
					return (
						<Table.Row
							key={task.id}
							onClick={() => navigate(`/system/tasks/${task.id}`)}
							className={`${row_bg} hover:bg-gray-50`}
						>
							<Table.Cell className="w-14">
								{ns_count > 0 && (
									<Badge
										color={"red"}
										className="rounded-full w-5 h-5 p-0 flex items-center justify-center"
									>
										{task.notifications_count}
									</Badge>
								)}
							</Table.Cell>
							<Table.Cell className="font-bold w-20">{task.id}</Table.Cell>
							<Table.Cell>{task.name}</Table.Cell>
							<Table.Cell className="w-[200px]">
								<TaskStatus status={task.status} />
							</Table.Cell>
							<Table.Cell className="w-32">{task.created_at}</Table.Cell>
							<Table.Cell className="w-14">
								<DotsHorizontalOI />
							</Table.Cell>
						</Table.Row>
					);
				})}
				{tasks.length > 0 && (
					<Table.Row className="bg-white w-full">
						<Table.Cell colSpan={6}>
							<TablePagination page={0} total={tasks.length} />
						</Table.Cell>
					</Table.Row>
				)}
			</Table.Body>
		</Table>
	);
}

export default function Tasks() {
	const SearchIcon = () => <SearchLoopOI className="text-gray-500 w-4 h-4" />;
	const [showModal, setShowModal] = useState(false);
	return (
		<div className="flex flex-col w-full p-6 bg-gray-100 h-full max-h-screen overflow-y-scroll">
			<div className="flex w-full justify-between pb-8">
				<div className="text-2xl font-semibold">Мои Обращения</div>
			</div>
			{/** Statistics */}
			<div className="flex flex-wrap  bg-white p-4 mb-6 self-stretch gap-4 items-stretch rounded-lg">
				<StatSection value={6} label={"Всего обращений"} color="gray">
					<ClipboardListOI className="w-10 h-10" />
				</StatSection>
				<StatSection value={2} label={"Завершено"} color="green">
					<CheckCircleOI className="w-10 h-10" />
				</StatSection>
				<StatSection value={1} label={"Требует вашего принятия"} color="purple">
					<ClockTimeOI className="w-10 h-10" />
				</StatSection>
				<StatSection value={2} label={"В работе"} color="yellow">
					<FireOI className="w-10 h-10" />
				</StatSection>
				<StatSection value={1} label={"На проверке"} color="blue">
					<EyeOI className="w-10 h-10" />
				</StatSection>
			</div>
			<div className="flex p-4 bg-white justify-between items-center self-stretch rounded-t-lg">
				<div className="flex items-center gap-4">
					<TextInput
						className="w-full min-w-[400px]"
						sizing={"sm"}
						placeholder="Поиск по обращениям"
						icon={SearchIcon}
					/>
					<Dropdown
						label="Фильтры"
						color="light"
						size={"xs"}
						dismissOnClick={false}
						className="p-5 rounded-lg"
						renderTrigger={() => (
							<Button size={"xs"} outline color="light">
								<FilterOI className="w-4 h-4" />
								<span className="ml-2">Фильтры</span>
							</Button>
						)}
					>
						<div className="flex flex-col gap-5 w-80">
							<span className="text-base font-semibold">
								Настройка фильтров
							</span>
							<div className="max-w-md">
								<div className="mb-2 block">
									<Label htmlFor="statuses" value="Статус" />
								</div>
								<Select id="statuses">
									<option>Все</option>
									<option>В работе</option>
									<option>Требует принятия</option>
									<option>На проверке</option>
									<option>Отклонено</option>
									<option>Завершено</option>
								</Select>
							</div>
							<Button color="blue" className="self-start" size={"sm"}>
								Применить
							</Button>
						</div>
					</Dropdown>
				</div>
				<Button size={"sm"} color="blue" onClick={() => setShowModal(true)}>
					<PlusOI className="w-4 h-4 mr-4" />
					Новое обращение
				</Button>
			</div>
			<TaskTable />
			{tasks.length == 0 && (
				<div className="w-[440px] mx-auto py-24">
					<div className="w-[268px] mb-10 mx-auto text-center text-zinc-500 text-xl font-medium">
						Здесь ещё нет обращений <br /> в техническую поддержку
					</div>
					<img src={EmptyTickets} alt="Empty Tasks" />
				</div>
			)}
			{showModal && (
				<TicketModal open={showModal} close={() => setShowModal(false)} />
			)}
		</div>
	);
}
