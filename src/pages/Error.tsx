
import NoDataAsset from "../assets/NoDataAsset.svg"



export default function Error() {

	return (
		<div className="h-screen w-full flex items-center justify-center">
			<div className="flex">
				<div className="max-w-2xl mr-24">
					<div className="w-80 text-black text-4xl font-bold leading-10 mb-8">
						Не можем найти ваши данные
					</div>
					<div className="w-96 text-xl">
						Пожалуйста, обратитесь в техническую поддержку по телефону
						<span className="text-indigo-700 text-xl font-semibold"
						>+7 (351) 2002 123</span
						>
					</div>
				</div>
				<img src={NoDataAsset} alt="" />
			</div>
		</div>

	)
}
