import { useEffect, useState } from "react"
import TelegramWidget from "../components/TelegramWidget"



const tasksByStatus = (task: any, status: string) => {
	return []
}

const profile = {
	name: "name",
	company: {
		name: "company"
	},
	branch: {
		name: "branch"
	}
}

const tasks: any[] = []



export default function SystemPage() {

	const [tickets, setTickets] = useState([])

	useEffect(() => {
		fetch("http://172.16.222.31:8055/items/tickets").then(response => {
			response.json().then(data => {
				console.log(data)
				setTickets(data.data)
			})
		})
	}, [])

	return (

		<div className="p-6">
			<h2 className="text-black text-2xl font-semibold mb-6">Мой профиль</h2>
			<div className="justify-start items-center gap-6 flex mb-10">
				<div
					className="h-24 w-24 bg-gradient-to-tr from-indigo-700 to-violet-400 rounded-full"
				/>
				<div>
					<div className="text-gray-900 text-base font-medium mb-4">
						{profile.name}
					</div>
					<div className="text-gray-500 text-sm leading-tight">
						<div className="mb-2">{profile.company.name}</div>
						<div>{profile.branch.name}</div>
					</div>
				</div>
			</div>

			<div className="mb-10">
				<h2 className="text-black text-2xl font-semibold mb-6">Обращения</h2>
				<div className="flex gap-4">
					<div
						className="px-10 pt-6 pb-8 bg-blue-50 rounded-xl flex-col items-center gap-4 flex"
					>
						<div className="text-blue-500 text-base font-bold">Создано</div>
						<div className="text-blue-900 text-5xl font-bold leading-10">
							{tickets.length}
						</div>
					</div>
					<div
						className="px-10 pt-6 pb-8 bg-green-50 rounded-xl flex-col items-center gap-4 flex"
					>
						<div className="text-green-500 text-base font-bold">Решено</div>
						<div className="text-green-900 text-5xl font-bold leading-10">
							{tasksByStatus(tasks, "done").length}
						</div>
					</div>
					<div
						className="px-10 pt-6 pb-8 bg-yellow-50 rounded-xl flex-col items-center gap-4 flex"
					>
						<div className="text-yellow-500 text-base font-bold">
							В работе
						</div>
						<div className="text-yellow-900 text-5xl font-bold leading-10">
							{tasksByStatus(tasks, "accepted").length}
						</div>
					</div>
				</div>
			</div>


			<div className="mt-4">
				<TelegramWidget />
			</div>
		</div>

	)
}
