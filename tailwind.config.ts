import type { Config } from 'tailwindcss'

const config: Config = {
	darkMode: 'class',
	content: [
		'./node_modules/flowbite-react/**/*.{js,ts,jsx,tsx,mdx}',
		'./src/**/*.{js,ts,jsx,tsx,mdx}',
		'./index.html'
	],
	theme: {
		extend: {
		},
	},
	plugins: [
		require("flowbite/plugin"),
	],
}
export default config
